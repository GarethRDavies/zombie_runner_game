﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public GameObject landingPad;
    public Transform playerSpawnPoints;

    private bool isRespawning = false;
    private Transform[] spawnPoints;
    private bool lastRespawnToggle = false;

	// Use this for initialization
	void Start () {
        spawnPoints = playerSpawnPoints.GetComponentsInChildren<Transform>();

    }
	
	// Update is called once per frame
	void Update () {
		if (lastRespawnToggle != isRespawning)
        {
            Respawn();
            isRespawning = false;
        }
	}

    private void Respawn()
    {
        int i = Random.Range(1, spawnPoints.Length);
        transform.position = spawnPoints[i].transform.position;
    }

    void OnFindClearArea()
    {
        Invoke("DropFlare", 3f);
    }

    void DropFlare()
    {
        Instantiate(landingPad, transform.position, Quaternion.identity);
    }
}
