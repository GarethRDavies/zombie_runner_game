﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayCycle : MonoBehaviour {

    [Tooltip ("Number of seconds per second that pass")]
    public float timeMultiplier = 60;

	
	// Update is called once per frame
	void Update () {
        float angleThisFrame = Time.deltaTime / 360 * timeMultiplier;
        transform.RotateAround(transform.position, Vector3.forward, angleThisFrame);
	}
}
