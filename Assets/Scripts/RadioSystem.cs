﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioSystem : MonoBehaviour {

    public AudioClip callSound;
    public AudioClip callReply;

    private AudioSource audioSource;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
	}

    void OnMakeInitialHeliCall()
    {
        audioSource.clip = callSound;
        audioSource.Play();

        Invoke("InitialCallReply", callSound.length + 1f);
    }

    void InitialCallReply()
    {
        audioSource.clip = callReply;
        audioSource.Play();
        BroadcastMessage("OnDispatchHelicopter");
    }
}
